#ifndef _WATCHDOG_h
#define _WATCHDOG_h

#define WATCHDOG_TIMEOUT WDTO_2S
#define WDT_TRIGGERED_BYTE 0xaf

void wdt_init(void);

#endif
