#include "functions.h"

uint8_t set_mux(uint8_t channel)
{
#if MUX_CHANNELS >= 4
  digitalWriteFast(MUX_S0, (channel & 1));
  digitalWriteFast(MUX_S1, ((channel >> 1) & 1));
#endif

#if MUX_CHANNELS == 8
  digitalWriteFast(MUX_S2, ((channel >> 2) & 1));
#endif
  return channel; // for master to query
}

void mux_init()
{
  Serial1.begin(9600);
#if MUX_CHANNELS >= 4
  digitalWriteFast(MUX_S0, LOW);
  digitalWriteFast(MUX_S1, LOW);
  pinModeFast(MUX_S0, OUTPUT);
  pinModeFast(MUX_S1, OUTPUT);
  // request a reading from each stamp then dump Serial1 RX
  for (int8_t i = 3; i >= 0; i--)
  {
    request_reading(i);
    delay(250);
  }
  delay(1200);
  while (Serial1.available() > 0)
  {
    uint8_t dump = Serial1.read();
  }
#endif

#if MUX_CHANNELS == 8
  digitalWriteFast(MUX_S2, LOW);
  pinModeFast(MUX_S2, OUTPUT);
  for (int8_t i = 7; i >= 4; i--)
  {
    request_reading(i);
    delay(250);
  }
  delay(1200);
  while (Serial1.available() > 0)
  {
    uint8_t dump = Serial1.read();
  }
#endif
}

uint8_t request_reading(uint8_t channel)
{
  set_mux(channel);
  Serial1.write('R');
  Serial1.write('\r');
  return channel;
}

void stamp_passthrough(uint8_t *char_buffer, size_t buf_len)
{
  for (uint8_t i = 2; i < buf_len; i++)
  {
    Serial1.write(char_buffer[i]);
  }
}

void leds_init()
{
  for (uint8_t i = LED_STARTPIN; i <= LED_LASTPIN; i++)
  {
    pinModeFast(i, OUTPUT);
    digitalWriteFast(i, LOW);
  }
}
