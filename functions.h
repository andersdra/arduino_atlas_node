#ifndef _FUNCTIONS_h
#define _FUNCTIONS_h

#include <stdint.h>
#include <digitalWriteFast.h>
#include "Arduino.h"

#define MUX_S0 A0
#define MUX_S1 A1

#define MUX_CHANNELS 4

#define LED_STARTPIN 4
#define LED_LASTPIN 9

void mux_init(void);
void leds_init(void);
uint8_t set_mux(uint8_t);
uint8_t request_reading(uint8_t);
void stamp_passthrough(uint8_t*, size_t);

#endif
