#ifndef _ARDUINO_ATLAS_NODE_h
#define _ARDUINO_ATLAS_NODE_h

#include "Arduino.h"
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <elapsedMillis.h>

// debug
#define DEBUG 0
#define CHECKSUM 1

// user definable
#define NUMBER_OF_PUMPS 4
#define PUMP_ON_STATE 1
#define PUMP_MULTIPLY_FACTOR 1000

#define TIMEOUT_LED 3
#define CHECKSUM_OK_LED 4
#define CHECKSUM_ERROR_LED 5
#define LED_ON_STATE 1

#define MASTER_RX_TIMEOUT_MS 2
#define LCD_UPDATE_HZ 5
#define NUMBER_OF_STAMPS 4

#define NUMBER_OF_LEDS 6
#define STAMP_LED_ON_MS 750
#define STAMP_LED_OFF_MS 250
#define STAMP_LED_BLINKS 1

#define STAMP_RX_BUFFER_SIZE 30
#define MASTER_RX_BUFFER_SIZE 30
/**/

// do not modify
#define LCD_UPDATE_TICKS (1000/LCD_UPDATE_HZ)
/**/

LiquidCrystal_I2C lcd(0x27, 20, 4);

elapsedMillis lcd_ticks;
elapsedMillis master_rx;
elapsedMillis stamp_rx;

elapsedMillis pump_timer0;
elapsedMillis pump_timer1;
elapsedMillis pump_timer2;
elapsedMillis pump_timer3;
elapsedMillis pump_timer[NUMBER_OF_PUMPS] = {pump_timer0, pump_timer1, pump_timer2, pump_timer3};

elapsedMillis led_timer0;
elapsedMillis led_timer1;
elapsedMillis led_timer2;
elapsedMillis led_timer3;
elapsedMillis led_timer4;
elapsedMillis led_timer5;
elapsedMillis led_timer[NUMBER_OF_LEDS] = {led_timer0, led_timer1, led_timer2, led_timer3, led_timer4, led_timer5};

static const uint8_t led_pin[NUMBER_OF_LEDS] = {4, 5, 6, 7, 8, 9};
static const uint8_t pump_pin[NUMBER_OF_PUMPS] = {10, 16, 14, 15};
uint8_t pump_divide_ratio[NUMBER_OF_PUMPS] = {12, 12, 12, 12};

typedef struct
{
  uint16_t on_ms = 0;
  uint16_t off_ms = 0;
  int8_t blinks = 0;
  uint8_t state = 0;
} status_t;

status_t led[NUMBER_OF_LEDS];

typedef struct
{
  uint32_t on_ms;
  uint8_t state;
} pumps_t;

pumps_t pump[NUMBER_OF_PUMPS];

// EEPROM
typedef struct
{
  uint8_t divide_ratio;
  int8_t pin;
} pump_t;

pump_t EEps[NUMBER_OF_PUMPS] EEMEM;
pump_t pump_setting[NUMBER_OF_PUMPS];

// UART
uint8_t rx_index = 0;
uint8_t rx_buf_master[MASTER_RX_BUFFER_SIZE];
/**/

uint8_t mux_channel = 0;

uint16_t lcd_value[NUMBER_OF_STAMPS] = {696, 1, 2, 3};
uint16_t lcd_value_last[NUMBER_OF_STAMPS] = {0, 0, 0, 0};

// protocol
#define ID_STAMP 0 //
//
#define STAMP_0 0
#define STAMP_1 1
#define STAMP_2 2
#define STAMP_3 3

#define ID_GET 1 //
//
#define GET_MUX 0

#define ID_SET 2 //
//
#define SET_MUX 0

#define ID_LCD 3 //

#define ID_PASSTHROUGH 4 //

#define ID_PUMP 5

#define ID_MISC 6
#define RESET 0

#endif
