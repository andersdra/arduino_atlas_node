/*
   Arduino Leonardo/ProMicro node for Atlas Scientific pH/EC stamps
   COBS encoded binary protocol with CRC16 checksum
*/
#include <avr/eeprom.h>
#include <avr/wdt.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <util/crc16.h>
#include "arduino_atlas_node.h"
#include "watchdog.h"
#include "cobs.h"
#include "functions.h"

void setup()
{
  wdt_disable();
  eeprom_read_block(&pump_setting, &EEps, sizeof(pump_t));
  for (uint8_t i = 0; i < NUMBER_OF_PUMPS; i++)
  {
    if (pump_setting[i].divide_ratio == 0xff) // eeprom initial value
    {
      pump_setting[i].divide_ratio = pump_divide_ratio[i];
    }
    if (pump_setting[i].pin == 0xff)
    {
      pump_setting[i].pin = pump_pin[i];
    }
  }
  eeprom_update_block(&pump_setting, &EEps, sizeof(pump_t)); // update pump settings

  for (uint8_t i = 0; i < NUMBER_OF_PUMPS; i++)
  {

    digitalWrite(pump_setting[i].pin, LOW);
    pinMode(pump_setting[i].pin, OUTPUT);
  }
  leds_init();
  lcd.begin();
  Serial.begin(115200);
  delay(1500); // let stamps power up properly
  mux_init();
  blink_led(0, 3, 500, 500);
  wdt_init();
}

void loop()
{
  wdt_reset();
  process_master();
  process_stamp();
  update_lcd();
  do_leds();
  do_pumps();
}

void process_stamp()
{
  if (Serial1.available() > 0)
  {
    uint8_t rx_buf_stamp[STAMP_RX_BUFFER_SIZE];
    size_t rx_length = (Serial1.readBytesUntil('\r', rx_buf_stamp, STAMP_RX_BUFFER_SIZE) + 3);

    if (rx_length == 0)
    {
      // timed out, or hit terminator before any data
      blink_led(TIMEOUT_LED, 2, 1000, 500);
    }

    uint16_t rxcrc = 0xffff;
    uint8_t out_buffer[rx_length];
    cobs_encode_result result;

    for (uint8_t i = 0; i < rx_length - 3; i++)
    {
      rxcrc = _crc_ccitt_update(rxcrc, rx_buf_stamp[i]);
    }
    rx_buf_stamp[rx_length - 3] = (uint8_t)(rxcrc >> 8);
    rx_buf_stamp[rx_length - 2] = (uint8_t)rxcrc;

    result = cobs_encode(out_buffer, rx_length, rx_buf_stamp, rx_length);

    for (uint8_t i = 0; i < result.out_len; i++)
    {
      Serial.write(out_buffer[i]);
    }
    Serial.write((uint8_t)0);
  }
}

void process_master()
{
  if (rx_index > 0)
  {
    if (master_rx >= MASTER_RX_TIMEOUT_MS)
    {
      blink_led(TIMEOUT_LED, 2, 250, 250);
      memset(rx_buf_master, 0, sizeof(rx_buf_master));
      rx_index = 0;
      master_rx = 0;
    }
  }

  if (Serial.available() > 0)
  {
    uint8_t rxchar = Serial.read();

    if (rxchar != 0)
    {
      rx_buf_master[rx_index] = rxchar;
      rx_index++;
      master_rx = 0; // reset timer between each char
    }
    else
    {
      if (rx_index)
      {
        uint16_t rxcrc = 0xffff;
        uint8_t out_buffer[rx_index + 1];
        cobs_decode_result result;

        result = cobs_decode(out_buffer, (rx_index + 1), rx_buf_master, rx_index);

        uint16_t checksum = ((uint16_t)out_buffer[result.out_len - 2] << 8) | out_buffer[result.out_len - 1];
        for (uint8_t i = 0; i < result.out_len - 2; i++)
        {
          rxcrc = _crc_ccitt_update(rxcrc, out_buffer[i]);
        }

        uint8_t rx_id = out_buffer[0];
        uint8_t rx_data = out_buffer[1];

#if CHECKSUM
        if (checksum == rxcrc)
        {
          blink_led(CHECKSUM_OK_LED, 1, 100, 0);
#endif
          switch (rx_id)
          {
            case ID_STAMP:
              {
                if (rx_data < NUMBER_OF_STAMPS)
                {
                  mux_channel = request_reading(rx_data);
                  blink_led(rx_data, STAMP_LED_BLINKS, STAMP_LED_ON_MS, STAMP_LED_OFF_MS);
                }
              } break;

            case ID_GET:
              {
                switch (rx_data)
                {
                  case GET_MUX:
                    {
                      send_byte(mux_channel);
                    } break;
                }
              } break;

            case ID_SET:
              {
                switch (rx_data)
                {
                  case SET_MUX:
                    {
                      uint8_t to_channel = out_buffer[2];
                      if (to_channel < MUX_CHANNELS)
                      {
                        uint8_t channel = set_mux(to_channel);
                        send_byte(channel);
                      }
                    } break;
                }
              } break;

            case ID_LCD:
              {
                uint16_t lcd_value = ((uint16_t)out_buffer[2] << 8 | out_buffer[3]);
                lcd_stamp(rx_data, lcd_value);
              } break;

            case ID_PASSTHROUGH:
              {
                if (rx_data < MUX_CHANNELS)
                {
                  mux_channel = set_mux(rx_data);
                  stamp_passthrough(out_buffer, result.out_len - 2);
                }
              } break;

            case ID_PUMP:
              {
                start_pump(rx_data, ((uint16_t)(out_buffer[2] << 8) | out_buffer[3]));
              } break;

            case ID_MISC:
              {
                switch (rx_data)
                {
                  case RESET:
                    {
                      while (1) { // force WDT reset
                        do_leds();
                      };
                    } break;
                }
              } break;
          }
#if CHECKSUM
        }
        else // wrong checksum
        {
          blink_led(CHECKSUM_ERROR_LED, 2, 75, 75);
          // request new data
        }
#endif
        memset(rx_buf_master, 0, sizeof(rx_buf_master));
        rx_index = 0;
      }
    }
  }
}

void send_byte(uint8_t tx_byte)
{
  uint16_t rxcrc = 0xffff;
  uint8_t out_buffer[5];
  cobs_encode_result result;

  rxcrc = _crc_ccitt_update(rxcrc, tx_byte);
  uint8_t tx_buf[4] = {tx_byte, (uint8_t)(rxcrc >> 8), (uint8_t)rxcrc};

  result = cobs_encode(out_buffer, 5, tx_buf, 3);

  for (uint8_t i = 0; i < result.out_len; i++)
  {
    Serial.write(out_buffer[i]);
  }
  Serial.write((uint8_t)0);
}

void lcd_stamp(uint8_t stamp_id, uint16_t value)
{
  if (stamp_id < NUMBER_OF_STAMPS)
  {
    lcd_value[stamp_id] = value;
  }
}

void update_lcd()
{
  if (lcd_ticks >= LCD_UPDATE_TICKS)
  {
    for (uint8_t i = 0; i < NUMBER_OF_STAMPS; i++)
    {
      if (lcd_value_last[i] != lcd_value[i])
      {
        lcd.setCursor(0, i);
        lcd.print(i);
        lcd.print(": ");
        lcd.print(lcd_value[i]);
        lcd.print("  ");
        lcd_value_last[i] = lcd_value[i];
      }
    }
    lcd_ticks = lcd_ticks - LCD_UPDATE_TICKS;
  }
}

void blink_led(uint8_t id, uint8_t blinks, uint16_t on_duration, uint16_t off_duration)
{
  digitalWriteFast(led_pin[id], HIGH);
  led[id].on_ms = on_duration;
  led[id].off_ms = off_duration;
  led[id].blinks = blinks - 1;
  led[id].state = LED_ON_STATE;
  led_timer[id] = 0;
}

void do_leds()
{
  for (uint8_t i = 0; i < NUMBER_OF_LEDS; i++)
  {
    if (led[i].state != LED_ON_STATE && led[i].blinks > 0)
    {
      if (led_timer[i] >= led[i].off_ms)
      {
        digitalWriteFast(led_pin[i], LED_ON_STATE);
        led[i].blinks--;
        led[i].state = LED_ON_STATE;
        led_timer[i] = 0;
      }
    }
    else
    {
      if (led[i].blinks >= 0)
      {
        if (led_timer[i] >= led[i].on_ms)
        {
          digitalWriteFast(led_pin[i], !LED_ON_STATE);
          led[i].state = !LED_ON_STATE;
          led_timer[i] = 0;
          if (led[i].blinks == 0)
          {
            led[i].blinks = -1;
          }
        }
      }
    }
  }
}

void start_pump(uint8_t id, uint16_t amount)
{
  digitalWriteFast(pump_setting[id].pin, PUMP_ON_STATE);
  pump[id].on_ms = (uint32_t)((amount * PUMP_MULTIPLY_FACTOR) / pump_setting[id].divide_ratio);
  pump[id].state = PUMP_ON_STATE;
  pump_timer[id] = 0;
}

void do_pumps()
{
  for (uint8_t i = 0; i < NUMBER_OF_PUMPS; i++)
  {
    if (pump[i].state == PUMP_ON_STATE)
    {
      if (pump_timer[i] >= pump[i].on_ms)
      {
        digitalWriteFast(pump_setting[i].pin, !PUMP_ON_STATE);
        pump[i].state = !PUMP_ON_STATE;
        pump[i].on_ms = 0;
      }
    }
  }
}
