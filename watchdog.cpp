#include "watchdog.h"
#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>

void wdt_init()
{
  uint8_t watchdog_reset = 0;
  // get byte from eep
  if (watchdog_reset == WDT_TRIGGERED_BYTE)
  {
    // read block to struct
    // send struct to host
    // reset trigger byte
  }
  wdt_enable(WATCHDOG_TIMEOUT);
  WDTCSR |= (1 << WDIE) | (1 << WDE) | (1 << WDCE);
}

// WDT is setup for interrupt + reset
// Save system state to EEPROM, force reset
ISR(WDT_vect)
{
  wdt_reset();
  // set led
  // set trigger byte
  // save essential data
  while (1); // reset
}
