#!/usr/bin/python3
'''
TODO:
message id?
data length byte
general function to send/read
'''

import serial
import serial.tools.list_ports
from cmd import Cmd
from cobs import cobs

ports = list(serial.tools.list_ports.comports())
for port in ports:
    if 'Leonardo' in port.description:
        print('node added: {}'.format(port.device))
        ser = serial.Serial(port.device, baudrate=115200, timeout=1.5, inter_byte_timeout=0.1, write_timeout=0.1)

def crc16_arr(bytelist):
    crc = 0xffff
    for a in range(len(bytelist)):
        crc ^= bytelist[a]
        for b in range(8):
            if (crc & 0x1):
                crc = (crc >> 1) ^ 0x8408
            else:
                crc = (crc >> 1)
    return crc

# extend for n data bytes to send
def node_passthrough(node, input):
    crc = crc16_arr([4, node, input, ord('\r')])
    ser.write(cobs.encode(bytearray([4, node, input, ord('\r'), ((crc >> 8) & 0xff), crc & 0xff])) + b'\x00')

def node_set(node):
   crc = crc16_arr([2, 0, node])
   ser.write(cobs.encode(bytearray([2, 0, node, ((crc >> 8) & 0xff), crc & 0xff])) + b'\x00')
   rx = ser.read_until(b'\x00')
   if rx:
       decoded = cobs.decode(rx[:-1])
       print(decoded[0])

def node_get(node):
    crc = crc16_arr([0, node])
    # request stamp reading
    ser.write(cobs.encode(bytearray([0, node, ((crc >> 8) & 0xff), crc & 0xff])) + b'\x00')
    # read node data
    rx = ser.read_until(b'\x00')
    if rx:
        decoded = cobs.decode(rx[:-1])
        # extract crc, convert to int
        rx_crc = int(decoded[-2:].hex(), 16)
        # calculate crc of received data
        calc_crc = crc16_arr(decoded[:-2])
        # get stamp '*OK'
        ok = cobs.decode(ser.read_until(b'\x00')[:-1])[:-2]
        # dump '*OK' and just check crc of data?
        if ok == b'*OK':
            if rx_crc == calc_crc:
                return cobs.decode(rx[:-1])
            else:
                return b''
        else:
            return b''

class node_prompt(Cmd):
    prompt = '> '
    intro = "stamp node tester"

    def do_exit(self, input):
        ser.close()
        return True

    def do_get(self, input):
        rx = node_get(int(input))
        if rx:
            reading = rx[:-2].decode()
            print(reading)
        else:
            print("no stamp?")

    def do_set_mux(self, input):
        node_set(int(input))

    def do_passthrough(self, input):
        cmd = input.split(',')
        node_passthrough(int(cmd[0]), ord(cmd[1]))

    def do_flush_input(self, input):
        ser.reset_input_buffer()

    def do_read_node(self, input):
        rx = ser.read_until(b'\x00')
        print(rx)

    def default(self, input):
        if input == 'q':
            return self.do_exit(input)
        print("default: {}".format(input))

    do_EOF = do_exit

if __name__ == '__main__':
   node_prompt().cmdloop()
